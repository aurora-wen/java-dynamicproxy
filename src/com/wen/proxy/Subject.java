package com.wen.proxy;

public interface Subject {
    public String doSomething();
}
