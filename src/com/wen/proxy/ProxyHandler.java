package com.wen.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/*
* 第一种方法
* 也是最简洁的
* */
public class ProxyHandler implements InvocationHandler {
    private Object tar;

    //绑定委托对象，并返回代理类
    public Object bind(Object tar){
        this.tar = tar;
        //绑定该类实现的所有接口，取代代理类
        //返回代理对象
        return Proxy.newProxyInstance(tar.getClass().getClassLoader(),
                                      tar.getClass().getInterfaces(),
                                       this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = null;

        //这里就可以进行所谓的AOP编程了
        // 在调用具体函数方法前，执行功能处理
        System.out.println(method.getName() + "方法开始执行...");

        result = method.invoke(tar,args);

        //在调用具体函数方法后，执行功能处理
        System.out.println(result);
        System.out.println(method.getName() + "方法执行结束...");

        return result;
    }
}
