package com.wen.proxy;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/*
* 第二种方法
* 该类主要用来体会方法的变化
* */
public class ProxyTest {

    /*
    * 使用 getProxyClass() 方法
    * 该方法不常用
    * */
    //target是目标对象
    public static Object getProxy0(final Object target) throws Exception{
        //获取代理Class类对象
        Class<?> proxyClass = Proxy.getProxyClass(target.getClass().getClassLoader(), target.getClass().getInterfaces());
        //获取构造器
        Constructor<?> constructor = proxyClass.getConstructor(InvocationHandler.class);
        //获取代理对象
        Object proxy = constructor.newInstance(new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                System.out.println(method.getName() + "方法开始执行。。。。。。。。");
                Object invoke = method.invoke(target, args);
                System.out.println(invoke);
                System.out.println(method.getName() + "方法执行结束。。。。。。。。");
                return invoke;
            }
        });
        return proxy;
    }


    /*
    * 一般使用Proxy类的另一个静态方法：
    * Proxy.newProxyInstance()
    * 该方法最常用
    * */
    public static Object getProxy1(final Object target) throws Exception{
        Object proxy = Proxy.newProxyInstance(target.getClass().getClassLoader(),  //类加载器
                target.getClass().getInterfaces(),  //让代理对象和目标对象实现相同接口
                new InvocationHandler() {  //代理对象的方法最终都会被JVM导向它的invoke方法
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        System.out.println(method.getName() + "方法开始执行。。。。。。。。");
                        Object invoke = method.invoke(target, args);
                        System.out.println(invoke);
                        System.out.println(method.getName() + "方法执行结束。。。。。。。。");
                        return invoke;
                    }
                }
        );
        return proxy;
    }
}
