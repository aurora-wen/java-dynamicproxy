package com.wen.proxy;

public class RealSubject implements Subject{
    @Override
    public String doSomething() {
        System.out.println("call doSomething()");
        return "hi,wen";
    }
}
