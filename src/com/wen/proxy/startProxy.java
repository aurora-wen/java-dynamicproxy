package com.wen.proxy;

import com.wen.proxy.ProxyTest;

public class startProxy {

    public static void main(String args[]) {
        /*
         * TestProxy
         * */
        ProxyHandler proxy = new ProxyHandler();
        //绑定该类实现的所有接口
        Subject sub = (Subject) proxy.bind(new RealSubject());
        sub.doSomething();


        /*
         * ProxyTest
         * getProxy0
         * */
        RealSubject target0 = new RealSubject();
        try {
            Subject proxy0 = (Subject) ProxyTest.getProxy0(target0);
            proxy0.doSomething();
        } catch (Exception e) {
            e.printStackTrace();
        }


        /*
         * ProxyTest
         * getProxy1
         * */
        RealSubject target1 = new RealSubject();
        try {
            Subject proxy1 = (Subject) ProxyTest.getProxy1(target1);
            proxy1.doSomething();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
